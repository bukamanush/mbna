Product comparision:

* Clone the project
* Run `composer install` being in the root folder
* Clone the `.env.example` into `.env` and create a 32 digit value for `APP_KEY` key
* Enter the `public` folder and run `npm install` and `bower install`
* Enter `public/app` folder and run `php -S localhost:8000`
* Visit `localhost:8000` and enjoy!
