(function () {
	'use strict';

	// Declare module
	angular.module('myApp.view1', ['ngRoute'])

	// Declare module configuration
	.config(['$routeProvider', function($routeProvider) {
		$routeProvider.when('/view1', {
			templateUrl: 'view1/view1.html',
			controller: 'View1Ctrl'
		});
	}])

	/**
	 * Product factory object.
	 */
	.factory('Products', ['$http', function ($http) {

		/**
		 * Get product data.
		 *
		 * @return {Promise}
		 */
		function get() {
			return $http.get('/api/products');
		}

		return {
			get: get
		};
	}])

	/**
	 * View1 controller.
	 */
	.controller('View1Ctrl', ['$scope', 'Products', function($scope, products) {
		// Store the current user selection
		$scope.example1model = [];

		// Get product list
		$scope.example1data = [];

		// Get products to render
		$scope.products = [];

		// Retrieve products from API
		products.get().then(function(products) {
			$scope.products = products.data.products;

			// Get product list to render options
			angular.forEach(products.data.products, function (product, i) {
				var item = {
					id: product.id,
					label: product.name
				};
				$scope.example1data.push(item);
			});
		});

		/**
		 * Check if a product is selected.
		 *
		 * @param  {integer}  productId
		 * @return {Boolean}
		 */
		$scope.isSelected = function(productId) {
			var selected = false;

			angular.forEach($scope.example1model, function (item, i) {
				if (item.id == productId) {
					selected = true;
				}
			});

			return selected;
		};

	}]);

})();
